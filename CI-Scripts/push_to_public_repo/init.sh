#!/bin/bash
#Author: Joakim Sørensen @ludeeus
#
# usage: init.sh [git] [repo] [subdir in the source repo] [subdir in remote repo]
# example: bash init.sh gitlab homeassistant-config /HA/PROD /config
#
# This script uses env_vars, you need to configure those:
# SH_USER = the username of the remote repo
# SH_API = the API key for that user
# SH_MAIL = your email adress

git=$1 || exit 1
repo=$2 || exit 1
subdir=$3
remotesubdir=$4

if [ ! -z "$remotesubdir" ]; then
    dir="/publish/$remotesubdir"
    mkdir /publish
    mkdir $dir
else
    dir="/publish"
    mkdir /publish
fi

if [ "$subdir" == "root" ]; then
    subdir=""
fi

git config --global user.name "$SH_USER" || exit 1
git config --global user.email "$SH_MAIL" || exit 1

sourcerepo=$(echo $PWD)

cd /publish || exit 1

git init
git remote add origin https://$SH_USER:$SH_API@$git.com/$SH_USER/$repo.git || exit 1
git fetch --all && git reset --hard origin/master

echo "Copying content"
cp -r $sourcerepo$subdir/* $dir

# When ready run:
# wget -q https://gitlab.com/ludeeus/toolbox/raw/master/CI-Scripts/push_to_public_repo/push.sh
# bash push.sh Europe/Oslo
#
# or:
# git add .
# git commit -m "message"
# git push -u origin master

echo "init.sh finished.."