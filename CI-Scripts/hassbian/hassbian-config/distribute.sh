#!/bin/bash
# Author: Joakim Sørensen @ludeeus
# Helper script to build and distribute hassbian-scripts.

branch=$1

if [ "$branch" == "dev"  ]; then
    REPO='repository-dev'
    timestamp=$(date +%s)
    CURRENTVERSION=$(cat ${CI_PROJECT_DIR}/package/DEBIAN/control | grep 'Version' | awk -F ' ' '{print $2}')
    VERSION=$(echo "${CURRENTVERSION}-dev-${timestamp}")
    sed -i 's/'$CURRENTVERSION'/'$VERSION'/g' ${CI_PROJECT_DIR}/package/DEBIAN/control
else
    REPO='repository'
fi

PACKAGE_VERSION=$(cat ${CI_PROJECT_DIR}/package/DEBIAN/control | grep 'Version' | awk -F ' ' '{print $2}')
apt-get install -y reprepro
mkdir /publish

git config --global user.name "$SH_USER" || exit 1
git config --global user.email "$SH_MAIL" || exit 1

echo "Adding package to repo..."
cd /publish || exit 1
git init
git remote add origin https://${SH_USER}:${SH_API}@gitlab.com/hassbian/$REPO.git || exit 1
git fetch --all && git reset --hard origin/master

reprepro --keepunreferencedfiles -S utils includedeb stretch ${CI_PROJECT_DIR}/package.deb

VERSION=$(cat /build/version)

git add -A
git commit -a -m "Automatic build and release for $PACKAGE_VERSION"
git push -u origin master

echo "Distribution complete..."