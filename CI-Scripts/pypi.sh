#!/bin/bash
# Author: Joakim Sørensen @ludeeus
#
# usage: pypi.sh
# example: bash pypi.sh
#
# This script uses env_vars, you need to configure those:
# PYPI_REPO = repository: https://upload.pypi.org/legacy/
# PYPI_USER = username: myuser
# PYPI_PASSWORD = password: y6vc73246asdasdu

version=$(cat ./setup.py | grep version | awk -F'"' '{print $2}')
pypi=$(curl -sSL https://pypi.org/pypi/${CI_PROJECT_NAME}/${version}/json | grep "Error code 404")

if [ ! -z "$pypi" ];then
	pip install --upgrade pip
	pip install twine setuptools
	rm -rf dist
	echo "[distutils]" >> ~/.pypirc
	echo "index-servers =" >> ~/.pypirc
	echo "    pypi" >> ~/.pypirc
	echo "" >> ~/.pypirc
	echo "[pypi]" >> ~/.pypirc
	echo "${PYPI_REPO}" >> ~/.pypirc
	echo "${PYPI_USER}" >> ~/.pypirc
	echo "${PYPI_PASSWORD}" >> ~/.pypirc
	python setup.py sdist bdist_wheel
	twine upload -r pypi dist/*.whl
else
    echo "The version $version exist on PyPi, no need to push."
fi