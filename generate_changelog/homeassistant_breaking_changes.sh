#!/bin/bash
#Author: Joakim Sørensen @ludeeus
releaseurl='https://api.github.com/repos/home-assistant/home-assistant.github.io/contents/source/_posts?ref=current'
releaseversion=$(curl -sSL https://pypi.org/pypi/homeassistant/json | jq .info.version | awk -F'.' '{print $2}')
controll=0
num=-1
while [[ controll -lt 10 ]]; do
  postname=$(curl -sSL -u "$GH_USER:$GH_API" $releaseurl | jq .[$num].name)
  if [[ "$postname" =  *"$releaseversion"* ]]; then
    filename=$(curl -sSL -u "$GH_USER:$GH_API" $releaseurl | jq .[$num].name | awk -F'"' '{print $2}')
    downloadurl="https://raw.githubusercontent.com/home-assistant/home-assistant.github.io/current/source/_posts/$filename"
    controll=10
  else
    num=$((num -1))
    controll=$((controll +1))
  fi
done
if [ -z "$downloadurl" ]; then
  echo "No post found..."
  exit 0
fi
file=$(echo "$downloadurl" | awk -F'/' '{print $NF}')
if [ -f $file ]; then
  rm $file
fi
wget -q $downloadurl

lineNum="$(grep -n 'linkable_title Breaking Changes' $file | head -n 1 | cut -d: -f1)"
cat $file | tail -n +$((lineNum+2)) | tee temp1 > /dev/null 2>&1
lineNum2="$(grep -n '## {' temp1 | head -n 1 | cut -d: -f1)"
cat temp1 | head -$((lineNum2-2)) | tee breaking_changes > /dev/null 2>&1
baseurl=https://github.com/home-assistant/home-assistant/pull/
echo "#### Breaking changes in 0.$releaseversion:" | tee -a current > /dev/null 2>&1
echo "" | tee -a current > /dev/null 2>&1
while read p; do
  context=$(echo $p | awk -F'- ' '{print $2}' | awk -F'(' '{print $1}')
  PR=$(echo $p | awk -F'(' '{print $2}' | awk -F')' '{print $1}' |  awk -F'[' '{print $3}' |  awk -F']' '{print $1}')
  echo " - ["$PR"]("$baseurl${PR:1}") - " $context "  " | tee -a current > /dev/null 2>&1
done <breaking_changes
date=$(echo $file | awk -F'.' '{print $1}' | awk -F'-' '{print $1"/"$2"/"$3"/"$4"-"$5}') > /dev/null 2>&1
echo "" | tee -a current > /dev/null 2>&1
echo "Blog post for this release: <https://www.home-assistant.io/blog/$date/#breaking-changes>" | tee -a current > /dev/null 2>&1
rm breaking_changes
rm temp1
cat current
rm current
rm $file